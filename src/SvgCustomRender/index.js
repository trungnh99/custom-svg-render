import { useEffect, useState, useRef } from "react";
import { ReactComponent as VietNamSvg } from "../vietnamHigh.svg";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import { Popover, Typography, IconButton } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import LoopIcon from "@material-ui/icons/Loop";

import "./styles.css";

const useStyles = makeStyles((theme) => ({
  popover: {
    pointerEvents: "none",
  },
  paper: {
    padding: theme.spacing(1),
  },
}));

function SvgCustomRender() {
  const [title, setTitle] = useState();
  const [openPopover, setOpenPopover] = useState(false);
  const [anchorEl, setAnchorEl] = useState();

  const elements = useRef(document.getElementsByClassName("land")).current;

  const toVietnamese = (text) => {
    if (!text) return;

    //convert Unicode
    const str = text.replace(/\\u[\dA-F]{4}/gi, function (match) {
      return String.fromCharCode(parseInt(match.replace(/\\u/g, ""), 16));
    });

    //convert ascii
    return str.replace(/\\x[\dA-F]{2}/gi, function (match) {
      return String.fromCharCode(parseInt(match.replace(/\\x/g, ""), 16));
    });
  };

  const onClick = (event) => {
    const id = event.currentTarget.getAttribute("id");
    const { title } = event.currentTarget.attributes;
    console.log("ID", id);
    console.log("attributes", typeof event.currentTarget.attributes, {
      title,
    });
    alert(toVietnamese(title.value));
  };

  const onMouseEnter = (event) => {
    setTitle(event.currentTarget.getAttribute("title"));
    setAnchorEl(event.currentTarget);
    setOpenPopover(true);
  };

  const onMouseLeave = (event) => {
    setOpenPopover(false);
  };

  //   const sleep = async (time) =>
  //     new Promise((resolve) =>
  //       setTimeout(() => {
  //         resolve();
  //       }, time)
  //     );

  useEffect(() => {
    const registerListener = () => {
      Array.from(elements).forEach((element) => {
        element.addEventListener("mouseenter", onMouseEnter);
        element.addEventListener("mouseleave", onMouseLeave);
        element.addEventListener("click", onClick);
      });
    };

    const removeEventListener = () => {
      Array.from(elements).forEach((element) => {
        element.removeEventListener("mouseenter", onMouseEnter, false);
        element.removeEventListener("mouseleave", onMouseLeave, false);
        element.removeEventListener("click", onClick, false);
      });
    };

    registerListener();

    return () => {
      //remove listen
      removeEventListener();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [elements]);

  const classes = useStyles();

  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <TransformWrapper>
        {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
          <div className="wrapper">
            <div className="tools">
              <IconButton color="primary" onClick={zoomIn}>
                <AddIcon />
              </IconButton>
              <IconButton color="secondary" onClick={zoomOut}>
                <RemoveIcon />
              </IconButton>
              <IconButton onClick={resetTransform}>
                <LoopIcon />
              </IconButton>
            </div>
            <TransformComponent>
              <VietNamSvg />
            </TransformComponent>
          </div>
        )}
      </TransformWrapper>
      <Popover
        className={classes.popover}
        classes={{
          paper: classes.paper,
        }}
        open={openPopover}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "center",
          horizontal: "left",
        }}
        // onClose={handlePopoverClose}
        disableRestoreFocus
      >
        <Typography>{toVietnamese(title)}</Typography>
      </Popover>
    </div>
  );
}

export default SvgCustomRender;
