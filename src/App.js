import SvgRender from "./SvgCustomRender";

function App() {
  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <SvgRender />
    </div>
  );
}

export default App;
