import { useRef, useEffect } from "react";
import * as echarts from "echarts";
import "echarts/lib/chart/map";
import "echarts/lib/component/tooltip";
import "echarts/lib/component/title";
import "echarts/lib/component/visualMap";
import VietNam from "./vietnam.json";

echarts.registerMap("VietNam", VietNam);

function VietNamMap() {
  const ref = useRef();

  useEffect(() => {
    const option = {
      title: {
        text: "Viet Nam",
        left: "right",
      },
      tooltip: {
        trigger: "item",
        showDelay: 0,
        transitionDuration: 0.2,
      },
      visualMap: {
        left: "right",
        min: 500000,
        max: 38000000,
        inRange: {
          color: [
            "#313695",
            "#4575b4",
            "#74add1",
            "#abd9e9",
            "#e0f3f8",
            "#ffffbf",
            "#fee090",
            "#fdae61",
            "#f46d43",
            "#d73027",
            "#a50026",
          ],
        },
        text: ["High", "Low"], // 文本，默认为数值文本
        calculable: true,
      },
      toolbox: {
        show: true,
        //orient: 'vertical',
        left: "left",
        top: "top",
        feature: {
          dataView: { readOnly: false },
          restore: {},
          saveAsImage: {},
        },
      },
      series: [
        {
          name: "Viet Nam",
          type: "map",
          roam: true,
          map: "VietNam",
          emphasis: {
            label: {
              show: true,
            },
          },
          data: [
            {
              name: "Nghệ An",
              value: 10000000,
            },
            {
              name: "Hà Nội",
              value: 30000000,
            },
          ],
        },
      ],
    };
    const myChart = echarts.init(ref.current, null, { renderer: "svg" });

    myChart.setOption(option);
  }, []);

  return (
    <div
      ref={ref}
      style={{
        width: "100%",
        height: 1000,
      }}
    ></div>
  );
}

export default VietNamMap;
